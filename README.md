# ci-demo

Simple JS project to demonstrate the usage of CI pipelines in gitlab.

The app here doesn't actually do anything and the setup is simple to demonstrate how CI can be used to build a project.

Also, the Dockerfile doesn't work or do anything either for the same reason.

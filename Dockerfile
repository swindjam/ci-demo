FROM node:16.14.0

COPY build build


# Note: this Dockerfile won't actually do anything and is just a demo of how to copy the files and expose the port
# In reality, this would need to have a server setup and use that (e.g. Nginx, or a Node JS script that runs a server)
CMD ["node", "-v"]

EXPOSE 8080
import add from './add';

describe('add.ts', () => {

    test('Should add 2 values', () => {
        expect(add(1, 2)).toEqual(3);
    });
});
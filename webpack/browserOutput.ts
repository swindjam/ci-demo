import context from './context';

export default {
    path: context + '/build',
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].[hash].js',
    publicPath: '/build'
}

export default [
    {
        test: /\.(ts|tsx)?$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader',
            options: {
                targets: {
                    // Browsers that support type="module"
                    browsers: [
                        'Chrome >= 61',
                        'Safari >= 11',
                        'iOS >= 11',
                        'Firefox >= 60',
                        'Edge >= 79'
                    ]
                }
            }
        }
    },
];
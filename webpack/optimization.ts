import TerserPlugin from 'terser-webpack-plugin';
import { MinifyOptions } from 'terser';

export default (minimize = true) => {
    let minimizer: (TerserPlugin<MinifyOptions> | CssMinimizerPlugin<CssMinimizerPlugin.CssNanoOptionsExtended>)[] = [];

    if (minimize) {
        minimizer = [
            new TerserPlugin({
                terserOptions: {
                    mangle: false,
                    keep_classnames: true,
                    keep_fnames: true
                }
            }),
        ];
    }

    return {
        splitChunks: {},
        minimize,
        minimizer
    };
};